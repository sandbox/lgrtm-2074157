<?php

/**
 * Implements hook_drush_command().
 */
function del_block_users_drush_command() {
  $items['del_block_users'] = array(
    'description' => dt('Delete all blocked users.'),
    'aliases' => array('dbu'),
    'examples' => array('drush del_block_users' => dt('Delete all blocked users.')),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => array('uids' => dt('Uids of users to not delete. Comma separated.')),
    'required-arguments' => FALSE,
  );
  return $items;
}


/**
 * Implements hook_drush_help().
 */
function del_block_users_drush_help() {
  return dt('Delete all blocked users. You can optionally exclude uids which should not be deleted.');
}

/**
 * Callback implements for del_block_users command.
 */
function drush_del_block_users($uids = '') {
  $role = user_role_load_by_name('spam');

  $users = db_select('users_roles', 'ur');
  $users->join('users', 'u', 'ur.uid = u.uid');

  $userspam = $users->fields('ur', array('uid'))
    ->condition('ur.rid', $role->rid, '=')
    ->condition('u.status', 0, '=')
    ->condition('u.uid', 1, '>')
    ->execute()
    ->fetchCol();

  if ($uids != '') {
    $usersexclude = array_map('drush_notdeleteuser', explode(",", $uids));
    $userspam = array_diff($userspam, $usersexclude);
  }

  $count = count($userspam);

  if ($userspam) {
    db_delete('users')
      ->condition('uid', $userspam, 'IN')
      ->execute();
      
    drush_log(dt("Deleted @count blocked users.", array('@count' => $count)), 'success');
  }
  else {
    drush_log(dt("There are not user to delete.", array('@count' => $count)), 'success');
  }
}

/**
 * helper function to contruct array.
 */
function drush_notdeleteuser($uid) {
  return $uid;
}