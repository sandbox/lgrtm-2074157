Drupal del_block_users module:
------------------------------
Maintainers:
Miguel Serrano (http://drupal.org/user/1929882)
Requires - Drupal 7
License - GPL (see LICENSE)


Overview:
--------
This module is designed to delete users due to spam register.


Features:
---------

* Drush command too.


Installation:
------------
You have clone the project in sites/all/modules with this:

1. git clone
--branch 7.x-1.x lgrtm@git.drupal.org:sandbox/lgrtm/2074157.git del_block_users.
2. Go to "Administer" -> "Modules" and enable it.



Configuration:
-------------
Remember clear caches.
Go to admin/people and select checkbox and erase buttom.

Optional: select the uid to protect from deletion. Multiple uids separated
for comas.

If you no select uids then delete all block users.
Caution for custom block users.


Drush:
------
drush dbu --help

A Drush command is provides for this funtcionality.

% drush del_block_users / drush dbu users
example:drush dbu

% drush del_block_users 'list of uids to no delete'
% drush dbu users 'list of uids to no delete'

example:drush del_block_users 3,8,9
example:drush dbu 3,8,9


TODO
====

Validation function.
Erase Confirmation should be in separate form.
Insert new role "span" in creation of new user to check in cron.
